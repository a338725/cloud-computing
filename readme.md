# Nginx
Aqui se realiza la creacion del servidor frontal Nginx, y los 3 contenedores de tipo docker-in-docker (dind),
para eso se usa un nginx.conf y docker-compose para contruila.

# Bastion
En este se realiza la configuracion de cada uno de los contenedores desde un playbook, donde se configuran
los contenedores para que, conectados por ssh, puedan crearse los 3 contenedores dentro de cada uno de los 
servidores web, luego para configurarlos con Nginx cada servidor.

# Notas de dind
- Para poder usar este tipo de contenedor hay que darles permisos de super usuario con --privileged en el run.
- No se ingresa como /bin/bash, este usa el comando -> docker exec -ti <nombre> sh
- El shell se basa en Alpine Linux, asi que los comandos son diferentes:
	-Para instalar cosas es:
		-apk update
		-apk add docker (para simular que el contenedor es un docker en si)
		-apk add python3
		-apk add openssh-server
		-apk add openrc (este es para los servicios)
		-apk add nano

-Una vez instalado todo, el contenedor puede tomar comandos de Linux Alpine y ademas los de docker, como
 docker run, start, etc.